sort_reads.py
qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza

qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv

## View demux.qzv

qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0 --p-trim-left-r 0\
      --p-trunc-len-f 173 --p-trunc-len-r 115\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs --o-table table --o-denoising-stats dns

qiime feature-table summarize\
      --i-table table.qza\
      --m-sample-metadata-file mdat.tsv\
      --o-visualization table

qiime feature-table summarize\
      --i-table table.qza\
      --m-sample-metadata-file mdat.tsv\
      --o-visualization table
qiime feature-table tabulate-seqs\
      --i-data rep-seqs.qza\
      --o-visualization rep-seqs

qiime feature-classifier classify-consensus-blast\
      --i-query rep-seqs.qza\
      --i-reference-taxonomy /usr/local/share/SILVA_databases/SILVA_128_QIIME_release/taxonomy/taxonomy_all/99/majority_taxonomy_all_levels.qza\
      --i-reference-reads /usr/local/share/SILVA_databases/SILVA_128_QIIME_release/rep_set/rep_set_all/99/99_otus.qza\
      --o-classification taxonomy\
      --p-perc-identity 0.8\
      --p-maxaccepts 1

qiime metadata tabulate\
      --m-input-file taxonomy.qza\
      --o-visualization taxonomy.qzv

qiime taxa barplot\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization taxa-barplots.qzv

qiime feature-table heatmap\
      --i-table table.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization heatmap.qzv\
      --m-metadata-column Ship

qiime taxa filter-table\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --p-include Diatomea\
      --o-filtered-table diatom-table

qiime taxa barplot\
      --i-table diatom-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization diatom-taxa-barplots.qzv


qiime taxa filter-table\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --p-include Bilateria\
      --o-filtered-table Bilateria-table

qiime taxa barplot\
      --i-table Bilateria-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization Bilateria-taxa-barplots.qzv
qiime alignment mafft\
      --i-sequences rep-seqs.qza\
      --o-alignment aligned-rep-seqs.qza\
      --p-n-threads 8
qiime alignment mask\
      --i-alignment aligned-rep-seqs.qza\
      --o-masked-alignment masked-aligned-rep-seqs.qza

qiime phylogeny fasttree\
      --i-alignment masked-aligned-rep-seqs.qza\
      --o-tree unrooted-tree.qza\
      --p-n-threads 8

qiime phylogeny midpoint-root\
      --i-tree unrooted-tree.qza\
      --o-rooted-tree rooted-tree.qza

qiime diversity core-metrics-phylogenetic\
      --i-phylogeny rooted-tree.qza\
      --i-table table.qza\
      --p-sampling-depth 3132\
      --m-metadata-file mdat.tsv\
      --output-dir core-metrics-results

qiime diversity alpha-group-significance\
      --i-alpha-diversity core-metrics-results/faith_pd_vector.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization core-metrics-results/faith-pd-group-significance.qzv

qiime diversity alpha-correlation\
      --i-alpha-diversity core-metrics-results/faith_pd_vector.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization core-metrics-results/faith-pd-correlation.qzv
qiime diversity beta-group-significance\
      --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza\
      --m-metadata-file mdat.tsv\
      --m-metadata-column Ship\
      --o-visualization core-metrics-results/unweighted-unifrac-Ship-significance.qzv\
      --p-pairwise

qiime diversity beta-group-significance\
      --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza\
      --m-metadata-file mdat.tsv\
      --m-metadata-column Control\
      --o-visualization core-metrics-results/unweighted-unifrac-Control-significance.qzv\
      --p-pairwise

qiime longitudinal pairwise-differences\
      --m-metadata-file mdat.tsv\
      --p-metric faith_pd\
      --p-state-column Control\
      --p-state-1 Yes\
      --p-state-2 No\
      --p-individual-id-column Ship\
      --p-group-column "Ship Present"\
      --m-metadata-file core-metrics-results/faith_pd_vector.qza\
      --o-visualization pairwise-differences.qzv
