mkdir  Project_MiFish/
cp mdat.tsv Project_MiFish/
cd Project_MDIBL12s
mkdir reads
cp */*.gz reads/
cd reads
ls -lh  ## Filenames
less *  ## Look at a read file
cd ..

source activate qiime2-2018.6
source tab-qiime

qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza
qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv

## View demux.qzv

qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0 --p-trim-left-r 0\
      --p-trunc-len-f 131 --p-trunc-len-r 127\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs --o-table table --o-denoising-stats dns

cd ..
cd Project_MDIBL12sD10
mkdir reads
cp */*.gz reads/
qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza
qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv

## View demux.qzv

qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0\
      --p-trim-left-r 0\
      --p-trunc-len-f 131\
      --p-trunc-len-r 127\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs\
      --o-table table\
      --o-denoising-stats dns

cd ../Project_MiFish

qiime feature-table merge\
      --i-tables ../Project_MDIBL12s/table.qza\
      --i-tables ../Project_MDIBL12sD10/table.qza\
      --p-overlap-method sum\
      --o-merged-table table.qza
qiime feature-table merge-seqs\
      --i-data ../Project_MDIBL12s/rep-seqs.qza\
      --i-data ../Project_MDIBL12sD10/rep-seqs.qza\
      --o-merged-data rep-seqs

qiime feature-table summarize\
      --i-table table.qza\
      --m-sample-metadata-file mdat.tsv\
      --o-visualization table
qiime feature-table tabulate-seqs\
      --i-data rep-seqs.qza\
      --o-visualization rep-seqs
qiime feature-classifier classify-consensus-blast\
      --i-query rep-seqs.qza\
      --i-reference-reads /data/share/databases/12S/12SnMito.fasta.qza\
      --i-reference-taxonomy /data/share/databases/12S/12SnMito.tax.qza\
      --o-classification taxonomy.qza\
      --p-perc-identity .8\
      --p-maxaccepts 1 
qiime metadata tabulate\
      --m-input-file taxonomy.qza\
      --o-visualization taxonomy.qzv
qiime taxa barplot\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization taxa-barplots.qzv
qiime feature-table heatmap\
      --i-table table.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization heatmap.qzv\
      --m-metadata-column Ship

qiime quality-control exclude-seqs\
      --i-query-sequences rep-seqs.qza\
      --i-reference-sequences /data/share/databases/12S/12SnMito.fasta.qza\
      --p-method blast\
      --p-perc-identity 0.8\
      --p-perc-query-aligned 0.30\
      --o-sequence-hits MiFish-rep-seqs.qza\
      --o-sequence-misses misses.qza

qiime feature-table filter-features\
      --i-table table.qza\
      --m-metadata-file MiFish-rep-seqs.qza\
      --o-filtered-table MiFish-table.qza

qiime taxa barplot\
      --i-table MiFish-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization MiFish-taxa-barplots.qzv
