# Downloading reads from the SRA
[Blog post on Fastq dump](https://edwards.sdsu.edu/research/fastq-dump/)

Let's go through how to download the reads and metadata for a [BioProject](https://www.ncbi.nlm.nih.gov/bioproject).

Start by searching bioproject for an interesting topic, I'm going to do "MiFish"

Then select a project, I chose one called [Seawater Metagenome](https://www.ncbi.nlm.nih.gov/bioproject/362566)

First let's download the Metadata: select the [BioSamples](https://www.ncbi.nlm.nih.gov/biosample?Db=biosample&DbFrom=bioproject&Cmd=Link&LinkName=bioproject_biosample&LinkReadableName=BioSample&ordinalpos=1&IdsFromResult=362566).

Then click send to; file; full and download the metadata file.  You can then upload it to the server from your computer using filezilla.

Next let's download the reads.  From the BioProject select the [SRA Experiments](https://www.ncbi.nlm.nih.gov/sra?linkname=bioproject_sra_all&from_uid=362566)

Select "Send to"; "file"; "Accession List"

Download that list, then upload it to the server.  

On the server we are going to use the fastq-dump command to download each accesssion from the list

### get_sra.sh
~~~bash
#!/bin/bash
filename="$1"
while read -r line
do
    fastq-dump --outdir reads --gzip --skip-technical  --readids --read-filter pass --dumpbase --split-3 --clip $line
done < "$filename"
~~~

To run it type:

~~~bash
get_sra.sh SraAccList.txt
~~~
And wait!