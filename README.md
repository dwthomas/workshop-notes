# T� Qiime 2 notes


We are going to go through a set of metabarcoding analysis on a cohort of samples taken here in Bar Harbour.
The sample were run with three different Primers, targeting three different clades.

####We ran:
1. 16S (515F-806R) targetting the V4 region of the 16S SSU rRNA, which should primarily amplify microbial prokaryotes.
2. 18S (Euk1391f-EukBr) targetting the 18S SSU rRNA, which should primarily amplify microbial eukaryotes.
3. 12S (MiFish) targetting the 12S rRNA, which primarily amplifies fish

#### Some links
[Qiime 2](https://qiime2.org/)

[Qiime 2 view](https://view.qiime2.org/)

[Overview of QIIME 2 Plugin Workflows](https://docs.qiime2.org/2018.6/tutorials/overview/#)

[Qiime 2 Documentation](https://docs.qiime2.org)

## 12S  

First we need to grab a copy of the demultiplexed data.  I've already downloaded it to the server, but it is exactly as you would get off our sequencer. 

~~~bash
cd                               # Make sure we're all in our home directory.
cp -r /usr/local/share/MDIBL_metabarcoding .  
cd MDIBL_metabarcoding
~~~

Now we'll have a look at the data, and prepare to bring it into qiime.  The 12S samples were each run with and without a 10 fold dilution.  So later we will want to merge those duplicate samples together.

~~~bash
mkdir  Project_MiFish/           # Make a directory for the MiFish
cp mdat.tsv Project_MiFish/      # put our metadata file in that directory
cd Project_MDIBL12s	  	         # Go into the folder with the undiluted data
mkdir reads 				     # Make a folder called "reads"
cp */*.gz reads/ 		         # and make a copy of all your read files, in the read folder
ls -lh reads 			         # look at the filenames
~~~

Now that we have all of the reads gathered into one folder it's easy to import them into qiime2! We always begin by activating the most recent qiime2 environment.  Right now that is qiime2-2018.6, but there is usually a new version every few months.  Qiime 2 uses a lot of other programs, dada2, blast, R, perl, python... All of which it requires specific versions of.  So we give Qiime 2 its own "sandbox" to play in, with the specific versions of everthing that it wants.  There are a couple of [installation](https://docs.qiime2.org/2018.6/install/) options, the one we use on the server is through a conda environment.  You can also download a virtual machine with qiime2 installed if you prefer!   

~~~bash
source activate qiime2-2018.6    # Activate the current qiime2-environment/sandbox
source tab-qiime                 # Load tab completion for qiime2
~~~

Now that we are in the Qiime 2 sandbox we are ready to [import!](https://docs.qiime2.org/2018.6/tutorials/importing/) These sequences are Paired End 250 run on a illumina HiSeq.  This is one of the only commands I would always reccoment copy-pasting, neither the type nor the source-format arguments tab-complete, and the camelcase makes it easy to make a class full of small spelling mistakes.  

~~~bash
qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza
~~~
Qiime2 uses two main filetypes:

1.	.qza "Qiime zipped archive"
2. .qzv "Qiime zipped visualization"

As the names suggest both of these types are just regular zip files.  All of them can be opened with [Qiime 2 View](https://view.qiime2.org/)  So lets make, download and view our first .qzv.

~~~bash
qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv
~~~
We want to use the demux.qzv to inform our decision on where to truncate our reads, during the trimming and quality filtering step.  The length of the MiFish amplicon varies but in general the longer sequences are around 240 bp.  So we want to truncate as much bad quality sequence as possible, while still keeping at least 240 bp + ~20 bp of overlap so that the reads can be joined!  In this case I chose to truncate the forward reads at 131 bp, and the reverse at 127 bp.


~~~bash
qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0 --p-trim-left-r 0\
      --p-trunc-len-f 131 --p-trunc-len-r 127\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs\
      --o-table table\
      --o-denoising-stats dns
~~~
Dada2 takes our reads, trims, filters, denoises, joins the paired end reads and performs chimera filtering.  Afterwords we get the set of all of the sequences in all of your samples, rep-seqs.qza.  The table mapping sequences and counts to samples, table.qza and the statistics of how many sequences made it through each step of dada2.  Lets have a look at how our samples faired.

~~~bash
qiime metadata tabulate\
      --m-input-file dns.qza\
      --o-visualization dns.qzv
~~~
That's one set imported!  Now let's do the same thing for the diluted samples.

~~~bash
cd ..
cd Project_MDIBL12sD10
mkdir reads
cp */*.gz reads/
qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza
qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv

## View demux.qzv

qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0\
      --p-trim-left-r 0\
      --p-trunc-len-f 131\
      --p-trunc-len-r 127\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs\
      --o-table table\
      --o-denoising-stats dns

~~~
Now it is time to combine the two sets of samples!  First lets go into that directory we made.

~~~bash
cd ../Project_MiFish
~~~
Then we start by merging the feature-tables.

~~~bash
qiime feature-table merge\
      --i-tables ../Project_MDIBL12s/table.qza\
      --i-tables ../Project_MDIBL12sD10/table.qza\
      --p-overlap-method sum\
      --o-merged-table table.qza
~~~
Then the rep-seqs.

~~~bash
qiime feature-table merge-seqs\
      --i-data ../Project_MDIBL12s/rep-seqs.qza\
      --i-data ../Project_MDIBL12sD10/rep-seqs.qza\
      --o-merged-data rep-seqs
~~~
Now we can visualize the feature table, and the rep-seqs.  rep-seqs.qzv will be particularily usefull, since it provides easy clickable links to a web blast for each individual sequence!

~~~bash
qiime feature-table summarize\
      --i-table table.qza\
      --m-sample-metadata-file mdat.tsv\
      --o-visualization table
qiime feature-table tabulate-seqs\
      --i-data rep-seqs.qza\
      --o-visualization rep-seqs
~~~
Next let's assign taxonomy to the sequences.

~~~bash
qiime feature-classifier classify-consensus-blast\
      --i-query rep-seqs.qza\
      --i-reference-reads /data/share/databases/12S/12SnMito.fasta.qza\
      --i-reference-taxonomy /data/share/databases/12S/12SnMito.tax.qza\
      --o-classification taxonomy.qza\
      --p-perc-identity .8\
      --p-maxaccepts 1 
~~~
And visualize them.

~~~bash     
qiime metadata tabulate\
      --m-input-file taxonomy.qza\
      --o-visualization taxonomy.qzv
qiime taxa barplot\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization taxa-barplots.qzv
qiime taxa collapse\
          --i-table table.qza\
          --i-taxonomy taxonomy.qza\
          --p-level 1\
          --o-collapsed-table taxa-table
qiime feature-table heatmap\
      --i-table taxa-table.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization heatmap.qzv\
      --m-metadata-column Ship
~~~
With MiFish we need to be careful to filter out spurious stuff that may be assigned incorrectly by blast when we are just picking the best percent identity hit.  

~~~bash
qiime quality-control exclude-seqs\
      --i-query-sequences rep-seqs.qza\
      --i-reference-sequences /data/share/databases/12S/12SnMito.fasta.qza\
      --p-method blast\
      --p-perc-identity 0.8\
      --p-perc-query-aligned 0.30\
      --o-sequence-hits MiFish-rep-seqs.qza\
      --o-sequence-misses misses.qza
qiime feature-table filter-features\
      --i-table table.qza\
      --m-metadata-file MiFish-rep-seqs.qza\
      --o-filtered-table MiFish-table.qza
qiime taxa barplot\
      --i-table MiFish-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization MiFish-taxa-barplots.qzv 
qiime feature-table heatmap\
      --i-table MiFish-table.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization MiFish-heatmap.qzv\
      --m-metadata-column Ship
~~~
And for reference [here are the species we found](https://docs.google.com/spreadsheets/d/1TpGjS96I0wL9RQg5WL_OF2nNYqQwuzBPNBJTINqPy84/edit?usp=sharing)!  



## 18S
The 18S data is going to be treated similarly to the MiFish data, except we have orders of magnitude more: reads, ASVs, taxonomic groups... simultaneously our database is substantially "worse".  With fish we could be pretty sure that we have a pretty close relative in the database.  That is not necissarily the case with 18S.

This time we got three .gz files off the sequencer.   R1, the forward reads, R2 the adapter, and R3 the reverse reads.  So to import the sequences into qiime easily we want to rename each of the R3 files to R2.  I've made a (very) little script to take care of that.  

~~~bash
sort_reads.py
~~~

~~~python
## sort_reads.py ##
import glob
import os
import shutil
os.mkdir("reads")
for f in glob.glob("*/*.gz"):
    shutil.copy2(f, "reads/" + f.split("/")[1])
for f in glob.glob("reads/*R3*"):
    os.rename(f, f.replace("R3", "R2"))
~~~

Now we're ready for Qiime!  Start by importing:

~~~bash
qiime tools import\
      --type 'SampleData[PairedEndSequencesWithQuality]'\
      --input-path reads\
      --source-format CasavaOneEightSingleLanePerSampleDirFmt\
      --output-path demux.qza

qiime demux summarize\
      --i-data demux.qza\
      --o-visualization demux.qzv
~~~
Based on the demux.qzv I chose to truncate the forward reads at 173 bp, and the reverse reads at 115 bp.

~~~bash
qiime dada2 denoise-paired\
      --i-demultiplexed-seqs demux.qza\
      --p-trim-left-f 0 --p-trim-left-r 0\
      --p-trunc-len-f 173 --p-trunc-len-r 115\
      --p-n-threads 72\
      --o-representative-sequences rep-seqs --o-table table --o-denoising-stats dns

qiime feature-table summarize\
      --i-table table.qza\
      --m-sample-metadata-file mdat.tsv\
      --o-visualization table
      
qiime feature-table tabulate-seqs\
      --i-data rep-seqs.qza\
      --o-visualization rep-seqs
~~~
For the taxonomy 

~~~bash
qiime feature-classifier classify-consensus-blast\
      --i-query rep-seqs.qza\
      --i-reference-taxonomy /data/share/databases/SILVA_databases/SILVA_132_QIIME_release/taxonomy/taxonomy_all/99/majority_taxonomy_all_levels.qza\
      --i-reference-reads /data/share/databases/SILVA_databases/SILVA_132_QIIME_release/rep_set/rep_set_all/99/silva132_99.qza\
      --o-classification taxonomy\
      --p-perc-identity 0.8\
      --p-maxaccepts 1

qiime metadata tabulate\
      --m-input-file taxonomy.qza\
      --o-visualization taxonomy.qzv

qiime taxa barplot\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization taxa-barplots.qzv

qiime feature-table heatmap\
      --i-table table.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization heatmap.qzv\
      --m-metadata-column Ship
~~~
Let's pull out just certain groups, diatoms, bilaterians..?

~~~bash
qiime taxa filter-table\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --p-include Diatomea\
      --o-filtered-table diatom-table
qiime taxa barplot\
      --i-table diatom-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization diatom-taxa-barplots.qzv
qiime taxa filter-table\
      --i-table table.qza\
      --i-taxonomy taxonomy.qza\
      --p-include Bilateria\
      --o-filtered-table Bilateria-table
qiime taxa barplot\
      --i-table Bilateria-table.qza\
      --i-taxonomy taxonomy.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization Bilateria-taxa-barplots.qzv
~~~      

Let's make heatmaps for all the different taxonomic levels.  We have 15 levels of taxonomy from silva, so it would be nice to not have to type 30 lines of qiime commands!

#####levels.sh

~~~bash
#! /bin/bash
mkdir heatmaps
for ((i = 1; i < 16; i++)); do
    qiime taxa collapse\
          --i-table table.qza\
          --i-taxonomy taxonomy.qza\
          --p-level $i\
          --o-collapsed-table heatmaps/level-$i-taxa-table
    qiime feature-table heatmap\
          --i-table heatmaps/level-$i-taxa-table.qza\
          --m-metadata-file mdat.tsv\
          --o-visualization heatmaps/level-$i-heatmap.qzv\
          --m-metadata-column Ship\
          --p-cluster features &
done
~~~
Notice the ampersand after the second command, we can put it in the background and start the next taxa-collapse while the previous visualization is still computing!  Lets make this script executable, then run it!

~~~bash
chmod +x levels.sh
./levels.sh
~~~


Create the phylogenetic tree

~~~bash
qiime alignment mafft\
      --i-sequences rep-seqs.qza\
      --o-alignment aligned-rep-seqs.qza\
      --p-n-threads 8
qiime alignment mask\
      --i-alignment aligned-rep-seqs.qza\
      --o-masked-alignment masked-aligned-rep-seqs.qza
qiime phylogeny fasttree\
      --i-alignment masked-aligned-rep-seqs.qza\
      --o-tree unrooted-tree.qza\
      --p-n-threads 8
qiime phylogeny midpoint-root\
      --i-tree unrooted-tree.qza\
      --o-rooted-tree rooted-tree.qza
~~~
Choose a rarefaction depth to normlize our samples to.  [Use table.qzv]

~~~bash
qiime diversity alpha-rarefaction\
      --i-table  table.qza\
      --p-max-depth 8000\
      --p-min-depth 10\
      --p-steps 30\
      --o-visualization alpha-rarefaction
qiime diversity core-metrics-phylogenetic\
      --i-phylogeny rooted-tree.qza\
      --i-table table.qza\
      --p-sampling-depth 3131\
      --m-metadata-file mdat.tsv\
      --output-dir core-metrics-results
~~~
Pick and visualize our favorite metrics...

~~~bash
qiime diversity alpha-group-significance\
      --i-alpha-diversity core-metrics-results/faith_pd_vector.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization core-metrics-results/faith-pd-group-significance.qzv

qiime diversity alpha-correlation\
      --i-alpha-diversity core-metrics-results/faith_pd_vector.qza\
      --m-metadata-file mdat.tsv\
      --o-visualization core-metrics-results/faith-pd-correlation.qzv
qiime diversity beta-group-significance\
      --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza\
      --m-metadata-file mdat.tsv\
      --m-metadata-column Ship\
      --o-visualization core-metrics-results/unweighted-unifrac-Ship-significance.qzv\
      --p-pairwise
qiime diversity beta-group-significance\
      --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza\
      --m-metadata-file mdat.tsv\
      --m-metadata-column Control\
      --o-visualization core-metrics-results/unweighted-unifrac-Control-significance.qzv\
      --p-pairwise
qiime longitudinal pairwise-differences\
      --m-metadata-file mdat.tsv\
      --p-metric faith_pd\
      --p-state-column Control\
      --p-state-1 Yes\
      --p-state-2 No\
      --p-individual-id-column Ship\
      --p-group-column "Ship Present"\
      --m-metadata-file core-metrics-results/faith_pd_vector.qza\
      --o-visualization pairwise-differences.qzv
~~~
