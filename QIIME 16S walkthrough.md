In this example we'll go over how to use QIIME 2 to analyze metabarcoding data.
These data are from set of mouse fecal samples provided by [Jason Bubier from The Jackson Laboratory](https://www.jax.org/research-and-faculty/faculty/research-scientists/jason-bubier).
The samples were extracted as part of a Bates College gut biology class taught by Train the Trainers alum Louise Brogan.
The samples were run targeting the V1-V2 regions of the 16S gene using the 27F/514R primer pair on an Illumnina MiSeq at The Jackson Lab on a paired end 300 bp run.
~~~
27F [20 bp]
 5'AGA GTT TGA TYM TGG CTC AG
515R [19 bp]
 5'TTA CCG CGG CKG CTG GCA C
~~~
For Metadata we have the sex, strain, whether the mouse has bred and age in days.
Our goal is to examine the correlation of the fecal microbiome we observe with these metadata.
We will primarily use [Qiime 2](https://qiime2.org/) a FOSS bioinformatics platform.
## Getting the Data
We start by activating the Qiime 2 environment.  The server is a shared resource and we may want to be able to use different version of programs, like blast or R or Python than Qiime 2 requires.  To enable this Qiime 2 is given its own working environment with the exact version of all the programs it requires.

~~~bash
#      version: qiime2-year.month
source activate qiime2-2019.4
~~~
Now lets grab a copy of the data!  Notice that the copy command will warn us that it is skipping the reads directory, that is OK!
~~~bash
mkdir mouse_fecal_example
cd mouse_fecal_example/
cp /home/share/examples/bates_mouse_fecal/* .
ls
# manifest.csv  mdat.tsv
~~~
The manifest file [manifest.csv] maps read files to samples for our analysis.  The metadata file [mdat.tsv] maps those samples to metadata about them.
~~~bash
less -S manifest.csv
# sample-id,absolute-filepath,direction
# 10-BATES,/home/share/examples/bates_mouse_fecal/reads/10-BATES_GT19-06009_AACTGAGG-GTATGCTG_S29_R1_001.fastq.gz,forward
# 10-BATES,/home/share/examples/bates_mouse_fecal/reads/10-BATES_GT19-06009_AACTGAGG-GTATGCTG_S29_R2_001.fastq.gz,reverse
# ...
~~~
If we look at the manifest file each sample has forward and reverse reads, denoted by the direction column.  Usually the filename will show this with R1 vs R2 for forward or reverse.
~~~bash
less -S mdat.tsv
# #SampleID       Sex     Strain  HaveBred        DOB     Days    Description
# 1-BATES F       Strain_1        mating  10/24/18        203     a5218
# 10-BATES        M       Strain_2        VIRGIN  7/8/18  310     15935
# 11-BATES        M       Strain_2        VIRGIN  7/8/18  310     15936
# ...
~~~
When we look at the metadata file we see the metadata that we will be able to use during our analysis.

Now we're ready to import the data into qiime. We will be using the qiime 2 command line interface, there is also a python interface called the Artifact API which can be a powerful tool.
~~~bash
# Anatomy of a qiime command
qiime plugin action\
   --i-inputs  foo\       ## input arguments start with --i
   --p-parameters bar\    ## paramaters start with --p
   --m-metadata mdat\     ## metadata options start with --m
   --o-outputs out        ## and output starts with --o
~~~
Qiime works on two types of files, Qiime Zipped Archives (.qza) and Qiime Zipped Visualizations (.qzv).  Both are simply renamed .zip archives that hold the appropriate qiime data in a structured format.  This includes a "provenance" for that object which tracks the history of commands that led to it.  The qza files contain data, while the qzv files contain visualizations displaying some data.
~~~bash
qiime tools import\
   --type 'SampleData[PairedEndSequencesWithQuality]'\
   --input-format PairedEndFastqManifestPhred33\
   --input-path manifest.csv\
   --output-path demux
   ## the correct extension is automatically added for the output by qiime.
~~~

## Quality Control and Denoising
Now we want to look at the quality profile of our reads.  Our goal is to determine how much we should truncate the reads before the paired end reads are joined.  This will depend on the length of our amplicon, and the quality of the reads.
~~~bash
qiime demux summarize\
   --i-data demux.qza\
   --o-visualization demux
~~~
When looking we want to answer these questions:

How much total sequence do we need to preserve an sufficient overlap to merge the paired end reads?

How much poor quality sequence can we truncate before trying to merge?

In this case we know our amplicons are about 500 bp long, and we want to preserve approximately 50 bp combined overlap.  So our target is to retain ~550 bp of total sequence from the two reads.  550 bp/2 = 275 bp but looking at the demux.qzv, the forward reads seem to be higher quality than the reverse, so let's retain more of the forward and less of the reverse.

We're now ready to denoise our data. Through qiime we will be using the program DADA2, the goal is to take our imperfectly sequenced reads, and reconstruct the "real" sequence composition of the sample that went into the sequencer.
DADA2 does this by learning the error rates for each transition between bases at each quality score.  It then attempts to recover the true sequences from the reads.  It begins by assuming that all of the sequences are errors off the same original sequence.  Then using the error rates it calculates the likelyhood of each sequence arising.  Sequences with a likelyhood falling below a threshold are split off into their own groups and the algorithm is iteratively applied.  Because of the error model we should only run samples which were sequenced together through dada2 together, as different runs may have different error profiles.  We can merge multiple runs together after dada2.  During this process dada2 also merges paired end reads, and checks for chimeric sequences.
~~~bash
qiime dada2 denoise-paired\
   --i-demultiplexed-seqs demux.qza\
   --p-trim-left-f 20 --p-trim-left-r 19\
   --p-trunc-len-f 290 --p-trunc-len-r 260\
   --p-n-threads 18\
   --o-denoising-stats dns\
   --o-table table\
   --o-representative-sequences rep-seqs
~~~
Now lets visualize the results of Dada2.
~~~bash
qiime feature-table tabulate-seqs\
   --i-data rep-seqs.qza\
   --o-visualization rep-seqs

qiime feature-table summarize\
   --i-table table.qza\
   --m-sample-metadata-file mdat.tsv\
   --o-visualization table

qiime metadata tabulate\
   --m-input-file dns.qza\
   --o-visualization dns
~~~
Looking at dns.qzv first we can see how many sequences passed muster for each sample at each step performed by dada2.  Here we are seeing great final sequence counts, and most of the sequences being filtered in the initial quality filtering stage.  Relatively few are failing to merge, which suggests we did a good job selecting our truncation lengths.

In the table.qzv we can see some stats on our samples.  We have 2.5 million counts spread accross 3,672 unique sequences and 47 samples.  We'll come back to the table.qzv file when we want to select our rarefaction depth.

In the rep-seqs.qzv we can see the sequences and the distribution of sequence lengths.  Each sequence is a link to a web-blast against the ncbi nucleotide database.  The majority of the sequences we observe are in our expected length range.
In this case we have many sequences around 270 bp as well.  These are usually valid sequences, from non target regions what the primer was able to amplify.  We will generally want to filter these out before creating a phylogenic tree, as they will not align well with the target sequences.
~~~bash
qiime quality-control exclude-seqs\
   --i-query-sequences rep-seqs.qza\
   --i-reference-sequences\
    /home/share/databases/SILVA_databases/SILVA_132_QIIME_release/rep_set/rep_set_all/99/silva132_99.qza\
   --p-method blast\
   --p-perc-identity 0.7\
   --p-perc-query-aligned 0.4\
   --o-sequence-hits filtered-rep-seqs.qza\
   --o-sequence-misses misses.qza
~~~
