# 16S Metabarcoding Example Using:

## Gut microbiome signature for cirrhosis due to nonalcoholic fatty liver disease: a prospective Twin and Family Study

### [UC San Diego NAFLD Research Center](https://medschool.ucsd.edu/som/medicine/divisions/gastro/research/NAFLD/research/patient-oriented/Pages/Twin-Family.aspx)

***

#### Obtaining Data

We start by locating, and then downloading data.  The National Center for Biotechnology Information "NCBI" is a great place to look.  We're going to start by looking in their BioProject database for a project.  In this case I searched "Cirrhosis" and picked the top result.  

[NCBI BioProject](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB28350)

At this time, these samples were submitted only a few weeks ago, and they appear to be a preliminary set of samples for an ongoing project.  We need to get two sets of data from the BioProject.  The demultiplexed reads, which contain the sequences detected by the sequencer.  And the metadata mapping their NCBI ID's (ERR2753615... ) to metadata (Cirrhosis, BMI...).  The Sequence Read Archive [SRA] is another NCBI database, linked from the BioProject which contains the reads associated with the project.  

[SRA](https://www.ncbi.nlm.nih.gov/sra?linkname=bioproject_sra_all&from_uid=487878)

We're going to use a command line tool, written by NCBI to download the reads for us.  

```bash
while read p; do
    fastq-dump --outdir fastq --gzip --skip-technical  --readids --read-filter pass --dumpbase --split-files --clip $p &
done <*.txt
```

Let's start looking at the middle line, the command fastq-dump is the tool we are using, and most of the rest of the line is boilerplate to download a single sample from the SRA, at the very end we have "$P" this is a variable p, which has the accesion number of the sample we want to download.  The ampersand tells this command to run "in the background" which means we will download all the samples at the same time, instead of waiting for one to finish before downloading the next (it makes a big difference!).

The "while _ ; do _ done" tells our program to run fastq-dump until we run out of samples to download.  And it knows what samples to download because we give it a list of accessions in a .txt file with the last line.

Now that we have the data, we need to download the metadata from another NCBI database.  You can download by selecting send to -> file -> format: full  We'll have a closer look at the metadata later.

[BioSample](https://www.ncbi.nlm.nih.gov/biosample?Db=biosample&DbFrom=bioproject&Cmd=Link&LinkName=bioproject_biosample&LinkReadableName=BioSample&ordinalpos=1&IdsFromResult=487878)

#### Bioinformatics!!  

We're going to be using qiime2, which is a powerful software package, that brings together many "state of the science" bioinformatics tools.  

[qiime2](https://qiime2.org/)

They have a very good community, and the developers, and other users are very active on the forums. We're going to start by looking at the metadata.  Qiime (pronounced chime) has a tool called keemei (pronounced like how you would mispronounce qiime)

[keemei](https://keemei.qiime2.org/)

This tool works as a google sheets addon, and makes sure your metadata is formatted in a valid way for qiime2.

[metadata](https://docs.google.com/spreadsheets/d/1-7Fy0yZWxmgIuWW_kM3GCtJLwcstHi2E_jWqev9eEbA/)

Now that we have some metadata we're going to go through a fairly simple analysis with qiime2, I usually do something similar with any given data set, then use that to inform what we focus in on.  This is going to be heavily based off of the main qiime tutorial: [Moving Pictures](https://docs.qiime2.org/2018.8/tutorials/moving-pictures/) there is also a smaller tutorial on FMT that you may find interesting [FMT tutorial](https://docs.qiime2.org/2018.8/tutorials/fmt/).

We'll start by importing and looking at our reads, we want to see good sequence quality that lasts throughout our reads, or our amplicon if it's shorter than the reads.  In this case we have single end 150 bp illumina MiSeq reads, which is modern next gen sequencing, and a great example of what you'd currently use for sequencing.  

[demux report](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Fdemux.qzv)

Informed by the demultiplexing report, the next thing we want to do is go through the sequences in each sample, and try and discern what the actual DNA sequences where.  We can see how many sequences made it through each step of QC in the [denoising stats](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Fdns.qzv).

It produces the [representative sequences](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Fe8404ca95782708d96a274f5708a466e43c4865c%2FLiver_files%2Frep-seqs.qzv), a list of unique sequences accross the samples.
And the [Feature Table](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Frep-seqs.qzv), which maps the representative sequences to the samples.  

#### Taxonomic analysis

Now that we have our sequences, we can align them against our database and try and assign taxonomy.  There are a few options for taxonomic assignment in qiime2, we're going to use NCBI-blast+ which aligns the sequences to the database, and picks the closest match to assign  [taxonomy](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Ftaxonomy.qzv).

With this we can make some very overwhelming  [barplots](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Ftaxa-barplots.qzv) with the addition of our table and metadata.  

We can also represent the same data as a [heatmap](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Fheatmap.qzv)

#### Phylogenetic analysis

If instead of focusing on specific taxonomy we look at the relative abundances of the sequences, and their relationship with each other we can get an idea of community diversity etc... without needing to know specific taxonomy for the sequences.

We can start by making a [Tree](https://itol.embl.de/tree/209222195223347231536587435) of how similar the sequences are.

We can examine if there are significant differences in [diversity](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Ffaith-group-significance.qzv) or [eveness](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Feveness-group-significance.qzv) for categorical metadata.

We can see if there is correlation between continuous metadata and [diversity](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Fc149d022e30991c1c1ce45f9bedc6c9b5fe45ae9%2FLiver_files%2Ffaith-pd-correlation.qzv) or [eveness](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Fc149d022e30991c1c1ce45f9bedc6c9b5fe45ae9%2FLiver_files%2Feveness-correlation.qzv).

We can also look at the difference within samples in metadata categories: [diversity](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2F7757804322cd051721bc5d2ac7575a47cfcd86b5%2FLiver_files%2Funweighted-unifrac-cirrhosis-significance.qzv)

And then we can look at [PCoA](https://view.qiime2.org/visualization/?type=html&src=https%3A%2F%2Fbitbucket.org%2Fdwthomas%2Fworkshop-notes%2Fraw%2Ffd40cbd32015eab624fb9a18d42355fdc7a6fa9f%2FLiver_files%2Funweighted_unifrac_emperor.qzv) plots to see if there are categories that drive changes it microbiome composition.  

##### Thanks!
I hope this was a useful introduction for you guys.  Though we did not actually "run" this analysis, you all are more than capable of doing it!
If you're interested in doing something similar, feel free to contact us!

Some useful links:
[NH Inbre](http://nhinbre.org/bioinformatics-genomics/)

[Hubbard Center For Genome Studies](https://hcgs.unh.edu/)
